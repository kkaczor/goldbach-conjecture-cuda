#include <stdio.h>
#include <stdlib.h>
#include <cuda_runtime.h>

__device__ int isPrime(int a)
{
	if (a == 1) return 0;
	if (a < 4) return 1;
	if (a % 2 == 0) return 0;
	if (a % 3 == 0) return 0;

	for (int i = 5; i * i <= a; i += 6)
	{
		if ((a % i == 0) || (a % (i + 2) == 0)) return 0;
	}
	return 1;
}

__device__ int checkGoldbachsConjecture(int a, int b, int id)
{
	int shouldPrint = 1;
	int isPossible = 0;
	print("a: ", a);
	print("b: ", b);
	if (a % 2 == 1) a += -1;
	if (b % 2 == 1) b += 1;
	for (; a <= b; a += 2)
	{
		for (int i = 2; i < a; i++)
		{
			if (isPrime(i) == 1)
			{
				if (isPrime(a - i) == 1)
				{
					isPossible = 1;
					if (shouldPrint == 1 && id < 10)
					{
						printf("\nHello this is thread %d, %d + %d = %d", id, a - i, i, a);
						shouldPrint = 0;
					}
					break;
				}
			}
		}

		if (isPossible == 0)
		{
			return 0;
		}

		isPossible = 0;
	}
	return 1;
}

__global__ void run(double* input, int* results, double* ranges)
{
	int blockIndex = blockIdx.x + blockIdx.y * gridDim.x;
	int myIndex = blockIndex * (blockDim.x * blockDim.y) + (threadIdx.y * blockDim.x) + threadIdx.x;

	ranges[2 * myIndex] = input[0] + input[2] * myIndex;
	ranges[2 * myIndex + 1] = input[0] + input[2] * myIndex + input[2];

	if (ranges[2 * myIndex] > input[1]) ranges[2 * myIndex] = input[1];
	if (ranges[2 * myIndex + 1] > input[1]) ranges[2 * myIndex] = input[1];

	results[myIndex] = checkGoldbachsConjecture((int)ranges[2 * myIndex], (int)ranges[2 * myIndex + 1], myIndex);
}

int main(int argc, char** argv)
{
	int finalResult = 1;
	double a = 40000000;
	double b = 50000000;
	int threadsInBlock = 100;
	int blocksInGrid = 256;
	int numberOfThreads = threadsInBlock * blocksInGrid;
	long size = sizeof(int) * numberOfThreads;
	double step = (b - a) / ((double)threadsInBlock * (double)blocksInGrid);

	double* input = (double*)malloc(sizeof(double) * 3);
	double* d_input;
	input[0] = a;
	input[1] = b;
	input[2] = step;
	cudaMalloc((void**)&d_input, sizeof(double) * 3);

	int* results = (int*)malloc(size);
	int* d_results;
	cudaMalloc((void**)&d_results, size);

	double* d_ranges;
	cudaMalloc((void**)&d_ranges, sizeof(double) * 2 * numberOfThreads);

	cudaMemcpy(d_input, input, sizeof(double) * 3, cudaMemcpyHostToDevice);

	// Launch kernel on GPU
	run << < blocksInGrid, threadsInBlock >> > (d_input, d_results, d_ranges);
	cudaDeviceSynchronize();

	// Get results
	cudaMemcpy(results, d_results, size, cudaMemcpyDeviceToHost);



	for (int i = 0; i < numberOfThreads; i++)
	{
		finalResult *= results[i];
	}

	if (finalResult == 1)
	{
		printf("\n Test PASSED: Goldabach was right \n");
	}
	else
	{
		printf("Test FAILED \n");
	}

	free(input);
	cudaFree(d_input);

	free(results);
	cudaFree(d_results);

	cudaFree(d_ranges);

	return 0;
}